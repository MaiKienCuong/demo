package maikiencuong.demospringbootelk.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class GenerateLogService {

    //cứ 10s ghi log một lần
    @Scheduled(fixedRate = 10000)
    public void info() {
        log.info("day la log info");
    }

    @Scheduled(fixedRate = 10000)
    public void warn() {
        log.warn("day la log warning");
    }

    @Scheduled(fixedRate = 10000)
    public void error() {
        log.error("day la log error");
    }

}