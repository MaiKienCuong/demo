package maikiencuong.demospringbootelk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class DemoSpringBootElkApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoSpringBootElkApplication.class, args);
    }

}
