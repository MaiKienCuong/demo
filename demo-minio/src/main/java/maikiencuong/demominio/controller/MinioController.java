package maikiencuong.demominio.controller;

import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;
import io.swagger.annotations.ApiOperation;
import maikiencuong.demominio.service.MinioService;
import maikiencuong.demominio.utils.FileTypeUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/minio")
public class MinioController {

    @Autowired
    private MinioService minioService;

    @PostMapping("/upload")
    @ApiOperation("Upload file")
    public String uploadFile(MultipartFile file, String bucketName) {
        String fileType = FileTypeUtils.getFileType(file);
        if (fileType != null) {
            return minioService.putObject(file, bucketName, fileType);
        }
        return " Unsupported file format . Please confirm the format , Upload again ！！！";
    }

    @PostMapping("/bucket/{bucketName}")
    @ApiOperation("Tạo bucket")
    public String addBucket(@PathVariable String bucketName) {
        try {
            minioService.makeBucket(bucketName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return " Create success ！！！";
    }

    @GetMapping("/bucket/{bucketName}")
    @ApiOperation("Lấy danh sách file trong bucket")
    public List<String> show(@PathVariable String bucketName) {
        try {
            return minioService.listObjectNames(bucketName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    @GetMapping("/bucket")
    @ApiOperation("Lấy danh sách các bucket")
    public List<String> showBucketName() {
        try {
            return minioService.listBucketName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    @GetMapping("/showListObjectNameAndDownloadUrl/{bucketName}")
    @ApiOperation("Trả về list file name và url download")
    public Map<String, String> showListObjectNameAndDownloadUrl(@PathVariable String bucketName) {
        Map<String, String> map = new HashMap<>();
        List<String> listObjectNames = null;
        try {
            listObjectNames = minioService.listObjectNames(bucketName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String url = "localhost:9000/minio/download/" + bucketName + "/";
        listObjectNames.forEach(System.out::println);
        for (int i = 0; i < listObjectNames.size(); i++) {
            map.put(listObjectNames.get(i), url + listObjectNames.get(i));
        }
        return map;
    }

    @DeleteMapping("/bucket/{bucketName}")
    @ApiOperation("Xóa bucket")
    public String delBucketName(@PathVariable String bucketName) {
        try {
            return minioService.removeBucket(bucketName) ? " Delete successful " : " Delete failed ";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @DeleteMapping("/removeObject/{bucketName}/{objectName}")
    @ApiOperation("Xóa file")
    public String delObject(@PathVariable("bucketName") String bucketName, @PathVariable("objectName") String objectName) {
        try {
            return minioService.removeObject(bucketName, objectName) ? " Delete successful " : " Delete failed ";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @DeleteMapping("/removeListObject/{bucketName}")
    @ApiOperation("Xóa danh sách các file")
    public String delListObject(@PathVariable("bucketName") String bucketName, @RequestBody List<String> objectNameList) {
        try {
            return minioService.removeListObject(bucketName, objectNameList) ? " Delete successful " : " Delete failed ";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @GetMapping("/download/{bucketName}/{objectName}")
    @ApiOperation("Download file")
    public void download(HttpServletResponse response, @PathVariable("bucketName") String bucketName, @PathVariable("objectName") String objectName) {
        InputStream in = null;
        try {
            in = minioService.downloadObject(bucketName, objectName);
            response.setHeader("Content-Disposition", "attachment;filename="
                    + URLEncoder.encode(objectName, StandardCharsets.UTF_8));
            response.setCharacterEncoding("UTF-8");

            IOUtils.copy(in, response.getOutputStream());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @GetMapping("/getLink/{bucketName}/{objectName}")
    @ApiOperation("")
    public String get(@PathVariable("bucketName") String bucketName, @PathVariable("objectName") String objectName) throws IOException, InvalidResponseException, InvalidKeyException, NoSuchAlgorithmException, ServerException, ErrorResponseException, XmlParserException, InsufficientDataException, InternalException {
        return minioService.getObjectUrl(bucketName, objectName);
    }
}