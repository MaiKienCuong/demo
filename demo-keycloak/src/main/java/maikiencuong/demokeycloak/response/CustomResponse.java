package maikiencuong.demokeycloak.response;

import lombok.Data;

@Data
public class CustomResponse {
    private int status;
    private String message;
}
