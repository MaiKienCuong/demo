package maikiencuong.demokeycloak.controller;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import maikiencuong.demokeycloak.dto.UserDTO;
import maikiencuong.demokeycloak.response.CustomResponse;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.authorization.client.AuthzClient;
import org.keycloak.authorization.client.Configuration;
import org.keycloak.authorization.client.util.Http;
import org.keycloak.representations.AccessTokenResponse;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping(value = "api/users")
public class UserController {

    @Value("${keycloak.auth-server-url}")
    private String authServerUrl;

    @Value("${keycloak.realm}")
    private String realm;

    @Value("${keycloak.resource}")
    private String clientId;

    @Value("${role.student.keycloak}")
    private String role;

    @Value("${client.secret.keycloak}")
    private String clientSecret;

    @Value("${admin.keycloak.username}")
    private String adminUsername;

    @Value("${admin.keycloak.password}")
    private String adminPassword;

    @PostMapping(path = "/register")
    @ApiOperation(value = "Đăng ký")
    public ResponseEntity<?> registerUser(@RequestBody UserDTO userDTO) {

        Keycloak keycloak = KeycloakBuilder.builder()
                .serverUrl(authServerUrl)
                .grantType(OAuth2Constants.PASSWORD)
                .realm("master")
                .clientId("admin-cli")
                .username(adminUsername)
                .password(adminPassword)
                .resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).build())
                .build();

        keycloak.tokenManager().getAccessToken();

        //tạo đối tượng user trên keycloak
        UserRepresentation user = new UserRepresentation();
        user.setEnabled(true);
        user.setUsername(userDTO.getEmail());
        user.setFirstName(userDTO.getFirstname());
        user.setLastName(userDTO.getLastname());
        user.setEmail(userDTO.getEmail());

        //lấy đối tượng realm, và users trên keycloak bằng tài khoản admin
        RealmResource realmResource = keycloak.realm(realm);
        UsersResource usersResource = realmResource.users();

        //tạo user
        Response response = usersResource.create(user);

        if (response.getStatus() == 201) {

            String userId = CreatedResponseUtil.getCreatedId(response);

            log.info("Created userId {}", userId);

            //reset password cho user
            CredentialRepresentation passwordCred = new CredentialRepresentation();
            passwordCred.setTemporary(false);
            passwordCred.setType(CredentialRepresentation.PASSWORD);
            passwordCred.setValue(userDTO.getPassword());

            UserResource userResource = usersResource.get(userId);
            userResource.resetPassword(passwordCred);

            RoleRepresentation realmRoleUser = realmResource.roles().get(role).toRepresentation();
            userResource.roles().realmLevel().add(Collections.singletonList(realmRoleUser));

            return ResponseEntity.ok(userDTO);
        }
        return ResponseEntity.status(response.getStatus()).build();
    }

    @PostMapping(path = "/login")
    @ApiOperation(value = "Đăng nhập")
    public ResponseEntity<?> login(@RequestBody UserDTO userDTO) {

        Map<String, Object> clientCredentials = new HashMap<>();
        clientCredentials.put("secret", clientSecret);
        clientCredentials.put("grant_type", "password");

        Configuration configuration = new Configuration(authServerUrl, realm, clientId, clientCredentials, null);
        AuthzClient authzClient = AuthzClient.create(configuration);

        try {
            AccessTokenResponse response = authzClient.obtainAccessToken(userDTO.getEmail(), userDTO.getPassword());
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            CustomResponse customResponse = new CustomResponse();
            customResponse.setStatus(401);
            customResponse.setMessage("Tài khoản hoặc mật khẩu không đúng");
            return ResponseEntity.status(401).body(customResponse);
        }

    }

    @GetMapping(path = "/refresh")
    @ApiOperation(value = "Refresh access token")
    public ResponseEntity<?> refreshAccessToken(@RequestParam String refreshToken) {

        Http http = new Http(new Configuration(authServerUrl, realm, clientId, null, null),
                (params, headers) -> {
                });

        String url = authServerUrl + "/realms/" + realm + "/protocol/openid-connect/token";

        AccessTokenResponse response = http.<AccessTokenResponse>post(url)
                .authentication()
                .client()
                .form()
                .param("grant_type", "refresh_token")
                .param("refresh_token", refreshToken)
                .param("client_id", clientId)
                .param("client_secret", clientSecret)
                .response()
                .json(AccessTokenResponse.class)
                .execute();
        return ResponseEntity.ok(response);
    }

    @GetMapping(value = "/unprotected-data")
    public String unProtectedData() {
        return "This api is not protected.";
    }

    @GetMapping(value = "/protected-data")
    @PreAuthorize("hasRole('student')")
    public String protectedData() {
        return "This api is protected.";
    }

}
